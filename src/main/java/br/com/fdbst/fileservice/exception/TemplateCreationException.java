package br.com.fdbst.fileservice.exception;

public class TemplateCreationException extends Exception {
    public TemplateCreationException(String message) {
        super(message);
    }
}
