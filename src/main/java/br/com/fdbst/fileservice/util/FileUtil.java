package br.com.fdbst.fileservice.util;

import org.springframework.stereotype.Component;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

@Component
public class FileUtil {

    private static final String BASE_PATH = "/quote/";

    public static String createFile(String name, String extension, byte[] byteArray) throws NoSuchAlgorithmException,
            IOException {
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        byte[] digest = messageDigest.digest((name + (new Date()).toString()).getBytes(StandardCharsets.UTF_8));
        BigInteger bigInt = new BigInteger(1, digest);
        String hashcode = bigInt.toString(16);
        while (hashcode.length() < 32) {
            hashcode = "0" + hashcode;
        }

        String filepath = BASE_PATH + hashcode + "." + extension;

        try (FileOutputStream stream = new FileOutputStream(filepath)) {
            stream.write(byteArray);
        } catch (IOException e) {
          throw e;
        }

        return filepath;
    }

}
