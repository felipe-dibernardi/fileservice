package br.com.fdbst.fileservice.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QuoteDTO {

    private String name;

    private String extension;

    private Integer expirationDays;

    private boolean oneUseOnly;

    private byte[] file;

}
