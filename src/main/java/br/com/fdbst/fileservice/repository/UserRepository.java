package br.com.fdbst.fileservice.repository;

import br.com.fdbst.fileservice.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User getUserByUsername(String username);

}
