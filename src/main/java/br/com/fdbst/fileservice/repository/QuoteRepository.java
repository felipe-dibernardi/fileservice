package br.com.fdbst.fileservice.repository;

import br.com.fdbst.fileservice.model.Quote;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface QuoteRepository extends JpaRepository<Quote, Long> {

    Optional<Quote> findQuoteByHashcode(String hashcode);

}
