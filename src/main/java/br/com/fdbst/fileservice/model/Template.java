package br.com.fdbst.fileservice.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "template")
public class Template {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Column(name = "file_path")
    private String filePath;

    @Column(name = "one_use_only")
    private boolean oneUseOnly;

}
