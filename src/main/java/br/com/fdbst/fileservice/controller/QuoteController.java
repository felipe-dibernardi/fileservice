package br.com.fdbst.fileservice.controller;

import br.com.fdbst.fileservice.dto.QuoteDTO;
import br.com.fdbst.fileservice.model.Quote;
import br.com.fdbst.fileservice.service.QuoteService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequiredArgsConstructor
@RestController
@RequestMapping("/quote")
public class QuoteController {

    private final QuoteService quoteService;

    @GetMapping()
    public ResponseEntity findQuoteByHashcode(@RequestParam("hashcode") String hashcode) {
        Optional<Quote> quoteOpt = quoteService.findQuoteByHashcode(hashcode);
        if (quoteOpt.isPresent()) {
            return ResponseEntity.ok(quoteOpt.get());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PostMapping("/new")
    public ResponseEntity createQuoteFromNewTemplate(@RequestBody QuoteDTO quoteDTO) {
        return ResponseEntity.ok().build();
    }

    @PostMapping("/template")
    public ResponseEntity createQuoteFromExistingTemplate(@RequestBody QuoteDTO quoteDTO) {
        return ResponseEntity.ok().build();
    }

}
