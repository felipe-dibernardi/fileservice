package br.com.fdbst.fileservice.service;

import br.com.fdbst.fileservice.dto.QuoteDTO;
import br.com.fdbst.fileservice.model.Quote;
import br.com.fdbst.fileservice.model.Template;
import br.com.fdbst.fileservice.repository.QuoteRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class QuoteService {

    private final QuoteRepository quoteRepository;

    private final TemplateService templateService;

    public Optional<Quote> findQuoteByHashcode(String hashcode) {
        return quoteRepository.findQuoteByHashcode(hashcode);
    }

    public void createQuoteFromNewTemplate(QuoteDTO quoteDTO) {

    }
}
