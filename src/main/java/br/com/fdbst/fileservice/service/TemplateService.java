package br.com.fdbst.fileservice.service;

import br.com.fdbst.fileservice.exception.TemplateCreationException;
import br.com.fdbst.fileservice.model.Template;
import br.com.fdbst.fileservice.repository.TemplateRepository;
import br.com.fdbst.fileservice.util.FileUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

@RequiredArgsConstructor
@Service
public class TemplateService {

    private final TemplateRepository templateRepository;

    public Template createTemplate(String name, String extension, byte[] byteArray, boolean oneUseOnly)
            throws TemplateCreationException {
        try {
            Template template = new Template();
            template.setName(name);
            template.setOneUseOnly(oneUseOnly);
            template.setFilePath(FileUtil.createFile(name, extension, byteArray));
            return templateRepository.save(template);
        } catch (IOException | NoSuchAlgorithmException e) {
            throw new TemplateCreationException("Error while creating template");
        }
    }

    public Template getTemplate(String name) {
        return null;
    }

}
