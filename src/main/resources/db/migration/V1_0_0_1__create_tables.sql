CREATE TABLE user (
    id INT(11) NOT NULL AUTO_INCREMENT,
    username VARCHAR(32) NOT NULL UNIQUE,
    password VARCHAR(64) NOT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB;

CREATE TABLE template (
    id iNT(11) NOT NULL AUTO_INCREMENT,
    name VARCHAR(64) NOT NULL,
    file_path VARCHAR(256) NOT NULL,
    one_use_only BOOLEAN DEFAULT TRUE,
    PRIMARY KEY (id)
) ENGINE=InnoDB;

CREATE TABLE quote (
    id INT(11) NOT NULL AUTO_INCREMENT,
    hashcode VARCHAR(256) NOT NULL,
    approved BOOLEAN DEFAULT FALSE,
    start_date DATETIME NOT NULL,
    end_date DATETIME NOT NULL,
    user_id INT(11) NOT NULL,
    template_id INT(11) NOT NULL,
    PRIMARY KEY (id),
    KEY fk_quote_user (user_id),
    KEY fk_quote_template (template_id),
    CONSTRAINT fk_quote_user FOREIGN KEY (user_id) REFERENCES user (id),
    CONSTRAINT fk_quote_template FOREIGN KEY (template_id) REFERENCES template (id)
) ENGINE=InnoDB;